Status: 
	Started with nRF5 SDK example “Nordic Blinky” in order to verify LED read/write commands and button notification 
	(located in the nRF5 SDK local folder, examples, ble_peripheral, blinky_app). Then adapted a tutorial from a 
	previous SDK version in order to add a custom service and characteristic to the GATT table, with a periodically 
	updating value of the internal temperature sensor.
	Ex: 5B-00-00-00 in hex = 91 units of 0.25°C = 22.75°C

NEXT STEPS:
•	Expand GATT table to make room for other data by copying method- minimal effort
•	Add external sensors and current drivers to GATT- substantial effort

Required Hardware:
•	nRF52840 DK
•	Micro-USB 2.0 cable
•	Computer to power dev kit
•	For testing
	•	Smartphone or Tablet with Nordic Connect App
	•	Second dev kit, or nRF52840 Dongle, with Nordic Connect for Desktop
	•	Or can use laptop for power and communication (with Visual Studio application) 

Required Software:
•	nRF5 SDK
	•	Note this is NOT the same as nRF SDK
	•	https://www.nordicsemi.com/Software-and-tools/Software/nRF5-SDK/Download#infotabs 
•	JLink Drivers
	•	https://www.segger.com/downloads/jlink/#JLinkSoftwareAndDocumentationPack 
•	SEGGER Embedded Studio (SES)
	•	https://www.segger.com/downloads/embedded-studio/ 

Transferring Firmware to Board: 
Method 1- Copy Hex file
•	When connect board to computer, a file directory should automatically open
	•	Similar to plugging in a USB Drive
•	Copy and paste precompiled hex file into board’s directory
	•	Hex files located in: Nan1931 Bitbucket -> Nordic Firmware repo ->master branch -> hex folder
	•	Choose the hex file that matches the board pca #
	•	If there are multiple hex files for that pca #, choose the compatible Soft Device s# (more information in Getting Started with nRF5 SDK and SES, nRF51 and 52 Series)
•	If correct program, LED4 should toggle on/off every second


Method 2- Programming Board
•	Navigate to the local copy of the nRF5 SDK
•	Then go to examples -> ble_peripheral
•	Create a new folder
•	Download the Nordic Firmware repository from the Nan1931 Bitbucket in the new folder
•	Open SEGGER Embedded Studio
•	Under File tab, select “Open Solution”
•	In the new folder with the Nordic Firmware repo, navigate to the correct board pca and soft device
•	Open the ses folder (SEGGER Embedded Studio) and open the solution
•	Then under the Build tab, select “Build and Run”

Recreating Firmware (if needed)
1.	In the nRF5 SDK examples, ble_peripheral, open ble_app_blinky
2.	Tutorial link
	a.	https://devzone.nordicsemi.com/nordic/short-range-guides/b/bluetooth-low-energy/posts/ble-services-a-beginners-tutorial 
	b.	This was made for the nRF51 series, but the nRF52840 is part of the nRF52 series
3.	From the GitHub referenced in the tutorial, copy:
	a.	our_service.c 
	b.	our_service.h 
	c.	Paste into the main ble_app_blinky folder (where main.c is stored)
4.	Then in main.c, copy and paste the lines of code:
		#include "our_service.h"
		#include "ble_advertising.h"
		#include "nrf_sdh_soc.h"
		#include "fds.h"
		#include "peer_manager.h
		#include <stdbool.h>
		#include <stdint.h>
		#include <string.h>
		//Note: omitted bsp_btn and sensorsim
		#define MANUFACTURER_NAME               "NordicSemiconductor"                   
		/**< Manufacturer. Will be passed to Device Information Service. */
		#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(5000)                   
		/**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
		#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(30000)                  
		/**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
		#define SEC_PARAM_BOND                  1         /**< Perform bonding. */
		#define SEC_PARAM_MITM                  0         /**< Man In The Middle protection not required. */
		#define SEC_PARAM_LESC                  0           /**< LE Secure Connections not enabled. */
		#define SEC_PARAM_KEYPRESS              0      /**< Keypress notifications not enabled. */
		#define SEC_PARAM_IO_CAPABILITIES    BLE_GAP_IO_CAPS_NONE /**< No I/O capabilities. */
		#define SEC_PARAM_OOB                   0                                 /**< Out Of Band data not available. */
		#define SEC_PARAM_MIN_KEY_SIZE          7                        /**< Minimum encryption key size. */
		#define SEC_PARAM_MAX_KEY_SIZE          16                     /**< Maximum encryption key size. */
5.	Then can follow the tutorial steps as listed
